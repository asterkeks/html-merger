FROM php:7.3

COPY builds/html-merger /usr/local/bin/html-merger

RUN mkdir /project
WORKDIR /project

ENTRYPOINT /usr/local/bin/html-merger html:merge
