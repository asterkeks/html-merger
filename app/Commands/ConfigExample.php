<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Filesystem\Filesystem;
use LaravelZero\Framework\Commands\Command;

class ConfigExample extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'config:example {target=.} {filename=merger.yaml}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Write example config';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Filesystem $filesystem)
    {
        $exampleConfig = base_path( 'assets/merger.yaml' );
        $targetConfig = $this->argument('target').'/'.$this->argument( 'filename' );
        $this->info("Copying example config to ${targetConfig}");
        copy($exampleConfig, $targetConfig);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
