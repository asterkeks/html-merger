<?php

namespace App\Commands;

use App\Merger\Exceptions\MergerException;
use App\Merger\Merger;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use LaravelZero\Framework\Commands\Command;
use App\Config\Loader\Loader as ConfigLoader;

class Merge extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'html:merge {config=merger.yaml}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Merge html pages to a single large html document';
	/**
	 * @var ConfigLoader
	 */
	private $configLoader;
	/**
	 * @var Merger
	 */
	private $merger;

	/**
	 * Merge constructor.
	 * @param ConfigLoader $configLoader
	 * @param Merger $merger
	 */
	public function __construct(ConfigLoader $configLoader, Merger $merger) {
		parent::__construct();
		$this->configLoader = $configLoader;
		$this->merger = $merger;
	}

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
	    $configPath = $this->argument('config');
    	$config = $this->configLoader->load($configPath);

    	if( Arr::has($config, 'base') )
    		$this->merger->setBasePath( Arr::get($config, 'base') );

    	$target = Arr::get($config, 'target', 'ebook.html');
	    $files = Arr::get($config, 'files', []);
	    $globalTransformers = Arr::get($config, 'transformer', []);
	    $this->merger->setGlobalTransformers($globalTransformers);
	    $headerTransformer = Arr::get($config, 'headerTransformer', []);
        $this->merger->setHeaderTransformer($headerTransformer);

	    try {

		    $this->merger
                ->merge($files)
				->into($target);

	    } catch(MergerException $e) {
	    	$this->error("Merging failed: ".$e->getMessage());
	    	return;
	    }

	    $this->info("Success! Merged into $target.");
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
