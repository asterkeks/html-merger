<?php namespace App\Config\Exceptions;

/**
 * Class FileNotFoundException
 * @package App\Config\Exceptions
 */
class FileNotFoundException extends LoaderException {
	/**
	 * @var string
	 */
	private $filePath;

	/**
	 * FileNotFoundException constructor.
	 * @param string $filePath
	 */
	public function __construct( string $filePath, int $code = 0, \Throwable $e = null) {
		parent::__construct("File $filePath not found", $code, $e);
		$this->filePath = $filePath;
	}

	/**
	 * @return string
	 */
	public function getFilePath(): string {
		return $this->filePath;
	}

}