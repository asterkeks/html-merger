<?php namespace App\Config\Loader;

use App\Config\Exceptions\FileNotFoundException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Loader
 *
 * Loads the configuration
 */
class Loader {

	/**
	 * @param string $configPath
	 * @return array
	 */
	public function load( string $configPath ) {

		if ( !file_exists( $configPath ) )
			throw new FileNotFoundException($configPath);

		$configContents = file_get_contents( $configPath );
		$config = Yaml::parse( $configContents );

		return $config;

	}

}