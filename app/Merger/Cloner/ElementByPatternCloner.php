<?php

namespace App\Merger\Cloner;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ElementByPatternCloner {

    /**
     * @var array[]
     **/
    protected $elements;

    protected $basePath = '';

    public function forElements($elements)
    {
        $this->elements = $elements;
        return $this;
    }

    public function setBasePath($basePath) : self
    {
        $this->basePath = $basePath;
        return $this;
    }

    public function handlePatterns() {
        $globbedPathes = [];
        foreach($this->elements as $element) {
            $globbedPathes[] = $this->handleFileGlob($element);
        }

        $pathes = [];
        foreach($globbedPathes as $globbedPath) {
            foreach($globbedPath as $path) {
                $pathes[] = $path;
            }
        };
        $this->elements = $pathes;
        return $this;
    }

    public function getClonedElements()
    {
        return $this->elements;
    }

    protected function handleFileGlob($element) {
        if( $this->isPatternAbsent($element) )
            return [$element];

        $fileNames = $this->globForFiles($element);

        $fileNames = $this->filterOutIgnoredFiles($element, $fileNames);

        $clonedEntries = $this->cloneElementPerFoundFileName($element, $fileNames);

        $sortedEntries = $this->sortByLastNumberInName($clonedEntries);

        return $sortedEntries;
    }

    private function isPatternAbsent($element)
    {
         return !Arr::has($element, 'pattern');
    }

    private function globForFiles($element)
    {
        $pattern = Arr::get($element, 'pattern.glob');

        $path = $this->basePath;
        $noSeparator = !Str::endsWith($path, DIRECTORY_SEPARATOR);
        if($noSeparator) {
            $path = $path.DIRECTORY_SEPARATOR;
        }
        $path = $path . $pattern;

        return collect( glob($path) );
    }

    private function filterOutIgnoredFiles($element, Collection $fileNames)
    {
        $ignoredFiles = collect(Arr::get($element, 'pattern.ignore', []));
        return $fileNames->filter(function($globbedPath) use ($ignoredFiles) {
            $fileName = basename($globbedPath);

            if( $ignoredFiles->contains($fileName) ) {
                return false;
            }

            return true;
        });
    }

    private function cloneElementPerFoundFileName($element, $fileNames)
    {
        return $fileNames->map(function($fileName) use ($element) {
            $name = basename($fileName);
            $pathClone = $element;
            $pathClone['name'] = $name;
            return $pathClone;
        });
    }

    private function sortByLastNumberInName($entries)
    {
        return $entries->sort(function($a, $b) {
            $aNumber = $this->findNumberInString($a['name']);
            $bNumber = $this->findNumberInString($b['name']);

            if($aNumber < $bNumber)
                return -1;
            if($aNumber > $bNumber)
                return 1;

            return 0;
        });
    }

    private function findNumberInString($string) {
        $matches = [];
        preg_match('~.*?(\d+).*~', $string, $matches);
        return $matches[1];
    }

}
