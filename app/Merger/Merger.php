<?php namespace App\Merger;

use App\Merger\Cloner\ElementByPatternCloner;
use App\Merger\Exceptions\NoFilesException;
use App\Merger\Transformer\TransformerFactory;
use Illuminate\Support\Arr;
use Symfony\Component\Console\Output\OutputInterface;
use App\Merger\Element\Loader\Loader as ElementLoader;
use App\Merger\Transformer\TakesElementList;

/**
 * Class Merger
 * @package App\Merger
 */
class Merger {

	/**
	 * @var string
	 */
	protected $basePath = '';

	/**
	 * @var OutputInterface
	 */
	protected $output;
	/**
	 * @var ElementLoader
	 */
	private $elementLoader;
	/**
	 * @var TransformerFactory
	 */
	private $transformerFactory;

	/**
	 * @var  array
	 */
	private $globalTransformers = [];

    /**
     * @var array
     **/
    private $headerTransformer = [];

    /**
     * @var array
     */
    private $nameToFile = [];

    /**
     * @var array[]
     */
    private $elements = [];

    /**
     * @var ElementByPatternCloner
     **/
    private $elementByPatternCloner;

	/**
	 * Merger constructor.
	 * @param ElementLoader $elementLoader
	 * @param TransformerFactory $transformerFactory
	 */
    public function __construct(
        ElementLoader $elementLoader,
        TransformerFactory $transformerFactory ,
        ElementByPatternCloner $elementByPatternCloner
    ) {
		$this->elementLoader = $elementLoader;
		$this->transformerFactory = $transformerFactory;
        $this->elementByPatternCloner = $elementByPatternCloner;
	}

	/**
	 * @param string $basePath
	 */
	public function setBasePath( string $basePath ): void {
		$this->basePath = $basePath;
	}

	/**
	 * @param OutputInterface $output
	 */
	public function setOutput( OutputInterface $output ) {
		$this->output = $output;
	}

    /**
     * @param array $elements
     */
	public function merge( array $elements ) {
	    $this->elements = $elements;

	    $elements = $this->elements;
        if ( empty( $elements ) )
            throw new NoFilesException();

        $firstFile = Arr::first( $elements );
        $this->buildDomFromFile( $firstFile );

        $this->elements = $this->handleFileGlobs( $elements );

        $this->buildNameToFileIndex();
        foreach ( $this->elements as $fileData )
            $this->appendFile( $this->dom, $fileData );

        return $this;
	}

    public function into($targetPath) {

        file_put_contents( $targetPath, $this->dom->htmlOuter() );
        return $this;
	}

    private function buildDomFromFile( $fromFile )
    {
        $element = $this->basePath . DIRECTORY_SEPARATOR . Arr::get( $fromFile, 'name' );
        $dom = $this->elementLoader->loadElements( $element, 'html' );
        $dom->find( 'body' )->empty();

        $this->transformHeadElement( $dom );

        $this->dom = $dom;
    }

    private function handleFileGlobs($elements)
    {
        return $this->elementByPatternCloner
            ->setBasePath($this->basePath)
             ->forElements($elements)
             ->handlePatterns()
            ->getClonedElements();

    }

	protected function appendFile( \phpQueryObject $dom, array $fileData ) {

        $fileName = Arr::get( $fileData, 'name' );
		$elementPath = $this->basePath . DIRECTORY_SEPARATOR . $fileName;
		$elements = Arr::get( $fileData, 'elements', []);

		$elements = $this->copyElementsFromOtherFile($elements, $fileData);

        $dom->find( 'body' )->append('<div id="file-'.$fileName.'"></div>');
		foreach ( $elements as $elementData ) {
			$element = $this->elementLoader->loadElements( $elementPath, Arr::get( $elementData, 'selector', 'body' ) );

			$this->transformBodyElement($element, $elementData);

            $onlyContent = Arr::get( $elementData, 'onlyContent', true );

            if( $onlyContent ) {
                $dom->find( 'body' )->append( $element->html() );
                continue;
            }
            $dom->find( 'body' )->append( $element->htmlOuter() );
			//$newElement->after('body');
		}

	}

    private function copyElementsFromOtherFile( $elements, array $fileData ) {
	    $hasElementsFromOtherFile = Arr::has($fileData, 'elementsFrom');
        $noElementsFromOtherFile = !$hasElementsFromOtherFile;
        if( $noElementsFromOtherFile )
            return $elements;

        $elementsFromOtherFile = Arr::get($fileData, 'elementsFrom');
        foreach($elementsFromOtherFile as $elementFromOtherFile) {
            $otherFileName = Arr::get($elementFromOtherFile, 'name');
            $otherFileElements = Arr::get($this->nameToFile[$otherFileName], "elements");

            $elements = array_merge($elements, $otherFileElements);
        }
        return $elements;
    }

	/**
	 * @param \phpQueryObject $element
	 * @param array $elementData
	 */
	protected function transformHeadElement(\phpQueryObject $dom) {
        $headElement = $dom->find( 'head' );

        $this->transformElement($headElement, $this->headerTransformer);
	}

	/**
	 * @param \phpQueryObject $element
	 * @param array $elementData
	 */
	protected function transformBodyElement(\phpQueryObject $element, array $elementData) {
		$elementTransformers = Arr::get($elementData, 'transformer', []);
		$transformers = array_merge($this->globalTransformers, $elementTransformers);

        $this->transformElement($element, $transformers);
	}

	/**
	 * @param \phpQueryObject $element
	 * @param array $elementData
	 */
	protected function transformElement(\phpQueryObject $element, array $transformers) {

		foreach($transformers as $transformerData) {
			$transformer = $this->transformerFactory->make( Arr::get($transformerData, 'name') );

            if($transformer instanceof TakesElementList) {
                $transformer->setElementList($this->elements);
            }
			$transformer->transform($element, $transformerData);
		}

	}

	/**
	 * @param array $globalTransformers
	 */
	public function setGlobalTransformers( array $globalTransformers ): void {
		$this->globalTransformers = $globalTransformers;
	}

	/**
	 * @param array $extraHeaders
	 */
	public function setHeaderTransformer( array $headerTransformer ): void {
		$this->headerTransformer = $headerTransformer;
	}

    private function buildNameToFileIndex() {
	    foreach($this->elements as $file) {
	        $this->addNameToFileEntry($file);
        }
    }

    private function addNameToFileEntry( array $file ) {
	    $fileName = Arr::get($file, 'name', '');

        $noName = empty( $fileName );
        if( $noName )
	        return;

        $this->nameToFile[$fileName] = $file;
    }

}
