<?php namespace App\Merger;

use Illuminate\Support\ServiceProvider;
use App\Merger\Transformer\LinkTarget\Transformer as LinkTargetTransformer;
use App\Merger\Transformer\Unique\Transformer as UniqueTransformer;
use App\Merger\Transformer\Delete\Transformer as DeleteTransformer;
use App\Merger\Transformer\Append\Transformer as AppendTransformer;
use App\Merger\Transformer\Index\Transformer as IndexTransformer;

/**
 * Class Provider
 * @package App\Merger
 */
class Provider extends ServiceProvider {

	/**
	 *
	 */
	public function register() {
		$this->app->bind('transformer.linktarget', LinkTargetTransformer::class);
		$this->app->bind('transformer.delete', DeleteTransformer::class);
		$this->app->bind('transformer.append', AppendTransformer::class);
		$this->app->bind('transformer.index', IndexTransformer::class);
		$this->app->singleton('transformer.unique', function() {
			return new UniqueTransformer();
		});
	}
}
