<?php namespace App\Merger\Exceptions;

/**
 * Class NoFilesException
 * @package App\Merger\Exceptions
 */
class NoFilesException extends MergerException {

}