<?php namespace App\Merger\Element\Loader;

use phpQuery;

/**
 * Class Loader
 * @package App\Merger\Element\Loader
 */
class Loader {

	/**
	 * @param $path
	 * @param $selector
	 * @return \phpQueryObject
	 */
	public function loadElements( $path, $selector ) {

		$dom = phpQuery::newDocumentFileHTML($path );

		return $dom->find($selector);

	}
}