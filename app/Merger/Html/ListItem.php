<?php namespace App\Merger\Html;

class ListItem {

    /**
     * @var TagSurrounder
     **/
    protected $tagSurrounder;

    protected $tag = 'li';

    protected $content = '';

    public function __construct(TagSurrounder $tagSurrounder) {
        $this->tagSurrounder = $tagSurrounder;
    }

    public static function fromContent($content) {
        /**
         * @var ListItem
         **/
        $listItem = app(self::class);
        $listItem->content = $content;

        return $listItem;
    }

    public function render() {
        return $this->tagSurrounder
             ->withTag($this->tag)
             ->surround($this->content);
    }
}
