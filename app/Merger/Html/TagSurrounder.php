<?php namespace App\Merger\Html;

class TagSurrounder {

    protected $tag;

    public function withTag($tag) {
        $this->tag = $tag;
        return $this;
    }

    public function surround($content) {
        $tag = $this->tag;
        return "<${tag}>".$content."</${tag}>";
    }
}
