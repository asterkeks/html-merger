<?php namespace App\Merger\Html;

class Anchor {

    protected $content;

    protected $linkTarget;

    public static function fromLink($linkTarget, $content) {
        /**
         * @var self $anchor
         **/
        $anchor = app(self::class);
        $anchor->linkTarget = $linkTarget;
        $anchor->content = $content;

        return $anchor;
    }

    public function render() {
        $content = $this->content;
        $linkTarget = $this->linkTarget;

        return "<a href=\"$linkTarget\">$content</a>";
    }
}
