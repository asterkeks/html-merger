<?php namespace App\Merger\Html;

use Illuminate\Support\Collection;

class UnorderedList {

    /**
     * @var Collection
     **/
    protected $items;

    public function __construct(TagSurrounder $tagSurrounder) {
        $this->tagSurrounder = $tagSurrounder;
        $this->items = collect();
    }

    /**
     * @return self
     **/
    public static function make() {
        return app(self::class);
    }

    public function render() {
        return $this->tagSurrounder
             ->withTag('ul')
             ->surround( $this->buildItems() );
    }

    protected function buildItems() {
        $items = $this->items->map(function(ListItem $item) {
            return $item->render();
        });

        return $items->implode("\n");
    }

    public function addItem($itemContent) {
        $item = ListItem::fromContent($itemContent);

        $this->items->push($item);
    }
}
