<?php namespace App\Merger\Transformer\Delete;

/**
 * Class Transfomer
 * @package App\Merger\Transformer\Unique
 */
class Transformer implements \App\Merger\Transformer\Transformer {

	/**
	 * @param \phpQueryObject $element
	 * @param array $data
	 */
	public function transform( \phpQueryObject $element, array $data ) {
		$selector = array_get($data, 'selector');

		$element->find($selector)->remove();
	}
}