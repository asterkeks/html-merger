<?php namespace App\Merger\Transformer\Index;

use App\Merger\Html\Anchor;
use App\Merger\Html\UnorderedList;
use App\Merger\Transformer\TakesElementList;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Transformer implements \App\Merger\Transformer\Transformer, TakesElementList {

    /**
     * @var Collection
     **/
    protected $elementList;

    /**
     * @var UnorderedList
     **/
    protected $index;

    public function __construct() {
        $this->elementList = collect();
    }

	/**
	 * @param \phpQueryObject $element
	 * @param array $data
	 */
	public function transform( \phpQueryObject $element, array $data ) {
        $indexName = Arr::get($data, 'name', 'index');

        $this->buildIndex();

        $element
            ->prepend( $this->index->render() )
            ->prepend( "<a id=\"$indexName\"></a>");
	}

    protected function buildIndex() {
        $this->makeIndex();
        $this->addElementsToIndex();
    }

    protected function makeIndex() {
        $this->index = UnorderedList::make();
    }

    protected function addElementsToIndex() {
        $this->elementList->each(function($element) {
            $elementContent = $this->buildElement($element);

            $this->index->addItem($elementContent);
        });
    }

    protected function buildElement($element) {
        $fileName = Arr::get($element, 'name', 'not-found');
        $link = '#file-'.$fileName;

        return Anchor::fromLink($link, $fileName)->render();
    }

    public function setElementList($elementList) {
        $this->elementList = collect($elementList);
    }

}
