<?php namespace App\Merger\Transformer\Unique;

use Illuminate\Support\Arr;

/**
 * Class Transfomer
 * @package App\Merger\Transformer\Unique
 */
class Transformer implements \App\Merger\Transformer\Transformer {

	/**
	 * @var bool
	 */
	protected $first = true;

	/**
	 * @param \phpQueryObject $element
	 * @param array $data
	 */
	public function transform( \phpQueryObject $element, array $data ) {
		$selector = Arr::get( $data, 'selector' );

		$hasElement = ( $element->find( $selector )->count() > 0 );
		if ( !$hasElement )
			return;

		if ( $this->first ) {
			$this->first = false;
			return;
		}

		$element->find( $selector )->remove();
	}
}
