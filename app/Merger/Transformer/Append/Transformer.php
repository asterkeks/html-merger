<?php namespace App\Merger\Transformer\Append;

use Illuminate\Support\Arr;

/**
 * Class Transfomer
 * @package App\Merger\Transformer\Unique
 */
class Transformer implements \App\Merger\Transformer\Transformer {

	/**
	 * @param \phpQueryObject $element
	 * @param array $data
	 */
	public function transform( \phpQueryObject $element, array $data ) {
		$content = Arr::get($data, 'content');

		$element->append($content);
	}
}
