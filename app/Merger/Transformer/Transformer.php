<?php namespace App\Merger\Transformer;

/**
 * Interface Transformer
 * @package App\Merger\Transformer
 */
interface Transformer {


	/**
	 * @param \phpQueryObject $element
	 * @param array $data
	 */
	function transform(\phpQueryObject $element, array $data);
}