<?php namespace App\Merger\Transformer;

use Illuminate\Container\Container;

/**
 * Class TransformerFactory
 * @package App\Merger\Transformer
 */
class TransformerFactory {

	/**
	 * @var string
	 */
	private $base = 'transformer.';

	/**
	 * @var Container
	 */
	private $container;

	/**
	 * TransformerFactory constructor.
	 * @param Container $container
	 */
	public function __construct( Container $container ) {
		$this->container = $container;
	}

	/**
	 * @param $name
	 * @param $data
	 * @return Transformer
	 */
	public function make( $name ) {
		return $this->container->make( $this->base . $name );
	}
}