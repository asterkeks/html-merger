<?php namespace App\Merger\Transformer\LinkTarget;

/**
 * Class Transformer
 * @package App\Merger\Transformer\LinkTarget
 */
class Transformer implements \App\Merger\Transformer\Transformer {

	/**
	 * @param \phpQueryObject $element
	 * @param array $data
	 */
	public function transform( \phpQueryObject $element, array $data ) {
		$pattern = array_get($data, 'pattern');
		$replacement = array_get($data, 'replacement');
		$element['a']->each(function(\DOMElement $link) use($pattern, $replacement) {
			$previousTarget = $link->getAttribute('href');
			$newTarget = preg_replace($pattern, $replacement, $previousTarget);

			$link->setAttribute('href', $newTarget);
		});
	}
}