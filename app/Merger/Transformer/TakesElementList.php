<?php namespace App\Merger\Transformer;

interface TakesElementList {

    function setElementList($elementList);

}
