# html-merger

Merge a plain old html file into a single large html document.

Use-case is to download the all guardsmen party via wget and merge them into a html file which can be converted into an ebook with calibre

## Usage
The whole merge process is governed by a merge.yaml config file. Use the following command to 

    html-merger config:example
   
To start the process of merging use the follwing command

    html-merger html:merge
   
### The HTML
The first thing the large html document needs is a html document skeleton.
    
### Selectors

### Transformers
